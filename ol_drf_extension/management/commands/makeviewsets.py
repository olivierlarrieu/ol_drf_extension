import os
import sys
import jinja2
from django.apps import apps
from django.core.management.base import BaseCommand


class DjangoModelsLister(object):
        
    def get_field(self, field):
        is_relation = False
        try:
            is_relation = field.related_model._meta.object_name
        except:
            pass
        return {
            'name': field.name,
            'is_relation': is_relation,
            'field': field
        }

    def get_model(self, model):          
        fields = [field for field in map(self.get_field, model._meta.get_fields())]
        return {
            'app_label': model._meta.app_label,
            'name': model._meta.object_name,
            'fields': fields
        }

    def get_models(self, app_name):
        models = apps.get_models()
        if app_name is None:
            return {
                model.get('name'): {'app_label': model.get('app_label'), 'fields': model.get('fields')}
                for model in map(self.get_model, models)
            }
        else:
            return {
                model.get('name'): {'app_label': model.get('app_label'), 'fields': model.get('fields')}
                for model in map(self.get_model, models) if model.get('app_label') == app_name
            }   

class ViewsetsGenerator(DjangoModelsLister):

    env = jinja2.Environment(loader=jinja2.PackageLoader('ol_drf_extension', 'templates'))
    template_viewset = env.get_template('viewset.tpl')
    template_urls = env.get_template('urls.tpl')
    template_test = env.get_template('tests.tpl')
    outputdir = 'output'
    app_name = None
    generated = {}

    def __init__(self, *args, **kwargs):
        if kwargs.get('outputdir') is not None:
            self.outputdir = kwargs.get('outputdir')
        if kwargs.get('app_name') is not None:
            self.app_name = kwargs.get('app_name')

    def mkdir(self):
        if os.path.isdir(self.outputdir):
            print('output dir exist, exit.')
            sys.exit(0)
        try:
            os.mkdir(self.outputdir)
        except IOError as e:
            raise e
        return True

    def generate(self):
        models = self.get_models(self.app_name)
        self.mkdir()
        for key, value in models.items():
            try:
                os.mkdir('{}/{}'.format(self.outputdir, value.get('app_label')))
                os.mkdir('{}/{}/viewsets'.format(self.outputdir, value.get('app_label')))
                os.mkdir('{}/{}/tests'.format(self.outputdir, value.get('app_label')))
            except:
                pass
            fields = [
                field.get('name') for field in value.get('fields')
                if not field.get('is_relation')
            ]

            with open('{}/{}/viewsets/{}_viewset.py'.format(self.outputdir, value.get('app_label'), key.lower()), 'w') as f:
                f.write(self.template_viewset.render(
                    {
                        'app_label': value.get('app_label'),
                        'model_name': key,
                        'fields': fields
                    })
                )
                print('created: {}/{}/viewsets/{}_viewset.py'.format(self.outputdir, value.get('app_label'), key.lower()))
                obj = {
                    "api_name": '{}'.format(key.lower()),
                    "filename": '{}_viewset.py'.format(key.lower()),
                    "import_name": '{}_viewset'.format(key.lower()),
                    "class_name": '{}Viewset'.format(key)
                }
                if self.generated.get(value.get('app_label')) is None:
                    self.generated[value.get('app_label')] = [obj]
                else:
                    self.generated[value.get('app_label')].append(obj)
                # self.generated.append(obj)

            with open('{}/{}/tests/tests_{}.py'.format(self.outputdir,value.get('app_label'), key.lower()), 'w') as f:
                f.write(self.template_test.render(
                    {
                        'class_name': key,
                        'class_name_lower': key.lower(),
                    })
                )
                print('created: {}/{}/tests/tests_{}.py'.format(self.outputdir, value.get('app_label'), key.lower()))
 
        return models

    def create_urls_file(self):
        for key, value in self.generated.items():
            with open('{}/{}/urls.py'.format(self.outputdir, key), 'w') as f:
                f.write(self.template_urls.render({"generated": value}))


class Command(BaseCommand):

    help = 'creates minimals serializers and model viewsets from your project models.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--app_name',
            dest='app_name',
            default=None,
            help='specifies specific app for processing.',
        )

    def handle(self, *args, **options):
        generator = ViewsetsGenerator(**options)
        generator.generate()
        generator.create_urls_file()
