from distutils.core import setup

setup(name='ol_drf_extension',
      version='1.0.5',
      description='Generate minimals Viewsets and Serializers from your project existing models',
      url='https://olivierlarrieu@bitbucket.org/olivierlarrieu/ol_drf_extension.git',
      author='Larrieu Olivier',
      author_email='larrieuolivierad@gmail.com',
      license='MIT',
      install_requires=['jinja2'],
      packages=['ol_drf_extension'],
      package_data = {'ol_drf_extension' : ['templates/*', 'management/*', 'management/commands/*'] },
)
